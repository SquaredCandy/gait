﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsEngineScript : MonoBehaviour
{
    [SerializeField]
    [ShowOnly] public List<GameObject> physicsObjects;

    public void FindPhysicsGameObjectsInLayer(int layer)
    {
        var goArray = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        foreach (GameObject go in goArray)
        {
            if (go.layer == layer && go.GetComponent<PhysicsData>() != null)
            {
                physicsObjects.Add(go);
            }
        }
    }
	
    void Start()
    {
        physicsObjects.Capacity = 400;
        FindPhysicsGameObjectsInLayer(8);
    }

	// Update is called once per frame
	void Update ()
    {
        // Update Motion of objects
        for(int i = physicsObjects.Count - 1; i >= 0; --i)
        {
            if(physicsObjects[i] == null)
            {
                physicsObjects.RemoveAt(i);
                continue;
            }
            PhysicsData pd = physicsObjects[i].GetComponent<PhysicsData>();
            Transform tf = physicsObjects[i].transform;
            ApplyTransform(tf, pd);
            ApplyRotation(tf, pd);
        }
	}

    void ApplyTransform(Transform tf, PhysicsData pd)
    {
        tf.position += new Vector3(pd.velocity.x, pd.velocity.y);
    }
    
    void ApplyRotation(Transform tf, PhysicsData pd)
    {
        tf.eulerAngles += new Vector3(0, 0, pd.rotation);
    }
}