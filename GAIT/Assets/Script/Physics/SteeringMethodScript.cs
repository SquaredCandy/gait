﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringMethodScript : MonoBehaviour {
    
    // Enum for the various steering methods
    public enum eSteeringMethod
    {
        Method1 = 0,
        Method2 = 1,
        Method3 = 2
    };

    // Holds the physics data to apply to object
    private PhysicsData pd;

    [Space(10)]
    // Distance from mouse to object
    [SerializeField]
    [ShowOnly]
    public float mouseDistance;
    // Current mouse position
    [SerializeField] [ShowOnly] public Vector3 mousePos;

    [Space(10)]
    // Current method of controlling the character
    public eSteeringMethod currentSteeringMethod = eSteeringMethod.Method1;

    // Is the object allowed to move? 
    // Used for steering method 5 only
    private bool bMove = false;

    private SteeringMethod[] steeringMethods = null;
    private Transform myTransform;
    private float dt;

	// Use this for initialization
	void Start () {
        myTransform = transform;
        pd = GetComponent<PhysicsData>();
        bMove = false;
        steeringMethods = new SteeringMethod[]
        {
            SteeringMethodOne,
            SteeringMethodTwo,
            SteeringMethodFive
        };
	}
	
	// Update is called once per frame
	public void UpdateSteeringBehaviour()
    {
        dt = Time.deltaTime;
        steeringMethods[(int)currentSteeringMethod]();

        // Cycle through all the steering methods
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            currentSteeringMethod = (eSteeringMethod)(((int)currentSteeringMethod + 1) 
                % steeringMethods.Length);
            // Reset the data so we don't bugs when switching
            pd.velocity = new Vector2(0, 0);
            pd.rotation = 0;
            myTransform.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    // Get mouse position to be used in steering methods
    // Also get the mouse distance
    void GetMousePosition()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        mouseDistance = Vector3.Distance(myTransform.position, mousePos);
    }

    void OnDrawGizmos()
    {
        if(myTransform != null && pd != null)
        {
            Gizmos.DrawLine(myTransform.position, myTransform.position + ((Vector3)pd.velocity.normalized));
        }
    }

    // Used as a function pointer definition so we can 
    // easily switch between different steering methods
    delegate void SteeringMethod();

    // WASD controls Up/Left/Down/Right | Forward Vector is static
    void SteeringMethodOne()
    {
        // Left / Right Movement
        float horizontalAxis = Input.GetAxis("Horizontal");
        // Up / Down Movement
        float verticalAxis = Input.GetAxis("Vertical");

        pd.velocity = (Vector2)((myTransform.right * horizontalAxis) 
            + (myTransform.up * verticalAxis)) 
            * pd.currentVelocity * dt;

        pd.currentVelocity = (horizontalAxis == 0 && verticalAxis == 0) 
            ? PhysicsUtility.LerpFloat(pd.currentVelocity, 0, pd.velocityDecel) 
            : PhysicsUtility.LerpFloat(pd.currentVelocity, pd.maxVelocity, pd.velocityAccel);
    }

    // WS Controls Forwards/Backwards | Forward Vector is controlled by AD
    void SteeringMethodTwo()
    {
        // Left / Right Rotation
        float horizontalAxis = Input.GetAxis("Horizontal");
        // Up / Down Movement
        float verticalAxis = Input.GetAxis("Vertical");

        pd.rotation = pd.currentRotation * -horizontalAxis * dt;
        pd.currentRotation = (horizontalAxis == 0) 
            ? PhysicsUtility.LerpFloat(pd.currentRotation, 0, pd.rotationDecel)
            : PhysicsUtility.LerpFloat(pd.currentRotation, pd.maxRotation, pd.rotationAccel);

        // Forwards & Backwards
        pd.velocity = (Vector2)(myTransform.up * verticalAxis) 
            * pd.currentVelocity * dt;
        pd.currentVelocity = (verticalAxis == 0) 
            ? PhysicsUtility.LerpFloat(pd.currentVelocity, 0, pd.velocityDecel)
            : PhysicsUtility.LerpFloat(pd.currentVelocity, pd.maxVelocity, pd.velocityAccel);
    }

    // Forward Vector is controlled by mouse Position WASD is 
    // Up/Left/Down/Right base on the forward vector
    void SteeringMethodThree()
    {
        GetMousePosition();
        PhysicsUtility.FacePosition(myTransform, mousePos, mouseDistance, pd);
        SteeringMethodOne();
    }

    // The object is constantly chasing after the mouse cursor
    void SteeringMethodFour()
    {
        GetMousePosition();
        PhysicsUtility.FacePosition(myTransform, mousePos, mouseDistance, pd);
        PhysicsUtility.MoveToPosition(myTransform, mouseDistance, pd);
    }

    // The object is chasing after where the player last clicked
    void SteeringMethodFive()
    {
        if(Input.GetMouseButtonDown(0) || Input.GetMouseButton(1))
        {
            GetMousePosition();
            bMove = true;
        }

        if(bMove)
        {
            PhysicsUtility.FacePosition(myTransform, mousePos, mouseDistance, pd);
            PhysicsUtility.MoveToPosition(myTransform, mouseDistance, pd);
            mouseDistance = Vector3.Distance(myTransform.position, mousePos);
            if (mouseDistance < pd.slowDistance)
            {
                bMove = false;
            }
        }
        else
        {
            pd.currentRotation = PhysicsUtility.LerpFloat(pd.currentRotation, 0, pd.rotationDecel);
            pd.currentVelocity = PhysicsUtility.LerpFloat(pd.currentVelocity, 0, pd.velocityDecel);
            pd.velocity = Vector3.Lerp(pd.velocity, new Vector3(0, 0, 0),
                pd.velocityDecel * dt);
            pd.rotation = PhysicsUtility.LerpFloat(pd.rotation, 0, pd.rotationDecel);
        }
    }
}