﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsUtility
{
    // Accelerate movementSpeed
    public static float LerpFloat(float current, float max, float accel)
    {
        return Mathf.Lerp(current, max, accel * Time.deltaTime);
    }

    // Make the object forward face the mouse cursor
    public static void FacePosition(Transform currentTrans, Vector3 targetPos, 
        float targetDistance, PhysicsData pd)
    {
        Vector3 target = (targetPos - currentTrans.position);
        float targetAngle = Vector2.SignedAngle(currentTrans.up, target);
        pd.rotation = targetAngle * pd.currentRotation * Time.deltaTime;
        pd.currentRotation = (targetDistance < pd.slowDistance)
            ? LerpFloat(pd.currentRotation, 0, pd.rotationDecel)
            : LerpFloat(pd.currentRotation, pd.maxRotation,
            pd.rotationAccel);
    }

    // Move the object to the mouse location
    public static void MoveToPosition(Transform currentTrans, float targetDistance, PhysicsData pd)
    {
        pd.velocity = (Vector2)(currentTrans.up) * pd.currentVelocity * Time.deltaTime;
        pd.currentVelocity = (targetDistance < pd.slowDistance)
                    ? LerpFloat(pd.currentVelocity, 0, pd.velocityDecel)
                    : LerpFloat(pd.currentVelocity, pd.maxVelocity, pd.velocityAccel);
    }
}
