﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour {

    private PlayerData pd;

	// Use this for initialization
	void Start () {
        pd = GetComponent<PlayerData>();
	}

    // If we get hit by a projectile we lose a life
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Projectile")
        {
            Destroy(col.gameObject);
            if(pd != null) pd.loseLife();
        }
    }
}
