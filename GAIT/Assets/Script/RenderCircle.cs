﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderCircle : MonoBehaviour {

    public LineRenderer lineRenderer;
    public int circlePrecision = 15;
    public float radius = 5;

    public void Start()
    {
        lineRenderer.positionCount = circlePrecision+1;
        AddLinePoints();
    }

    void AddLinePoints()
    {
        float TwoPi = Mathf.PI * 2;
        float increase = TwoPi / circlePrecision;
        int i = 0;
        for (float theta = 0; theta < TwoPi; theta += increase)
        {
            Vector2 pos = new Vector3(Mathf.Cos(theta), Mathf.Sin(theta));
            lineRenderer.SetPosition(i++, pos * radius);
        }
    }
}
