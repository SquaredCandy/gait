﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalSpawner : MonoBehaviour {

    public GameObject snitch;
    public GameObject goal;
    public PlayerData pd;
    public Text scoreText;

    public PhysicsEngineScript pec;

    public int endScore = 1;
    public float spawnRange = 95;

    private GameObject currentSnitch;
    private GameObject currentGoal;

    public GameObject screenObject;
    public GameObject scoreObject;
    public GameObject livesObject;
    public GameObject endGameTextObject;
    public GameObject endGameScoreTextObject;

    // We spawn the snitch and the goal
	void Start () {
        currentSnitch = Instantiate(snitch);
        currentGoal = Instantiate(goal, Random.insideUnitCircle * spawnRange, 
            Quaternion.identity);
        scoreText.text = "Score: 0";
        pec.physicsObjects.Add(currentSnitch);
    }
	
	// Check if there has been a goal scored, if so we respawn the goal and snitch
	void Update () {
		if(Vector2.Distance(currentGoal.transform.position, currentSnitch.transform.position) 
            <= currentGoal.GetComponent<RenderCircle>().radius)
        {
            Destroy(currentGoal);
            Destroy(currentSnitch);
            
            scoreText.text = "Score: " + (++pd.score).ToString();

            currentSnitch = Instantiate(snitch);
            currentGoal = Instantiate(goal, Random.insideUnitCircle * spawnRange,
                Quaternion.identity);
            pec.physicsObjects.Add(currentSnitch);
        }

        if(pd.score + pd.currentLives >= endScore)
        {
            Camera c = Camera.main;
            int height = c.pixelHeight;
            int width = c.pixelWidth;

            RectTransform screen = screenObject.GetComponent<RectTransform>();
            screen.sizeDelta = new Vector2(width, height);

            if (pd.currentLives == 0) pd.currentLives = 1;
            float endScoref = (float)pd.score / (float)pd.currentLives;
            Text endScore = endGameScoreTextObject.GetComponent<Text>();
            endScore.text = "Final Score: " + endScoref;

            endGameTextObject.SetActive(true);
            endGameScoreTextObject.SetActive(true);
            screenObject.SetActive(true);
            scoreObject.SetActive(false);
            livesObject.SetActive(false);


            Time.timeScale = 0;

        }
	}
}