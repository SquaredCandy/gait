﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpawner : MonoBehaviour {

    public List<Transform> spawnLocations;
    public List<GameObject> spawnables;

    public float spawnRate = 2;
    public int maxSpawn = 100;
    private int spawned = 0;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("SpawnBuff", 0, spawnRate);
    }

    // We continuously spawn enemies until we reach a max
    void SpawnBuff()
    {
        int locIndex = Random.Range(0, spawnLocations.Count);
        {
            if (RandomBool())
            {
                var go = Instantiate(spawnables[Random.Range(0, spawnables.Count)], gameObject.transform) as GameObject;
                go.transform.position = spawnLocations[locIndex].transform.position;
                if (++spawned >= maxSpawn)
                {
                    CancelInvoke("SpawnBuff");
                }
            }
        }
    }

    bool RandomBool()
    {
        return (Random.value > 0.5f);
    }
}