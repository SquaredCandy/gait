﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public List<Transform> spawnLocations;
    public List<GameObject> spawnables;

    public PhysicsEngineScript pec;

    public float spawnRate = 2;
    public int maxSpawn = 100;
    private int spawned = 0;

	// Use this for initialization
	void Start () {
        InvokeRepeating("SpawnEnemy", 0, spawnRate);
	}

    // We continuously spawn enemies until we reach a max
    void SpawnEnemy()
    {
        foreach (Transform location in spawnLocations)
        {
            if (RandomBool())
            {
                var go = Instantiate(spawnables[Random.Range(0, spawnables.Count)], gameObject.transform) as GameObject;
                go.transform.position = location.transform.position;
                go.GetComponent<FiniteStateMachine>().Start();
                AIData ai = go.GetComponent<AIData>();
                ai.destination = Random.insideUnitCircle *
                    Random.Range(ai.minSearchRadius, ai.maxSearchRadius);
                pec.physicsObjects.Add(go);

                GunScript gs = go.GetComponent<GunScript>();
                if (gs != null)
                {
                    gs.pec = pec;
                }

                if(++spawned >= maxSpawn)
                {
                    CancelInvoke("SpawnEnemy");
                }
            }
        }
    }

    bool RandomBool()
    {
        return (Random.value > 0.5f);
    }
}
