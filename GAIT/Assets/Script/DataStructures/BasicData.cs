﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicData : MonoBehaviour {

    // The Leader we are following
    public GameObject followingLeader;
    // The radius at which we start following the leader
    public float findLeaderRadius = 5;

    // The offset to the leader the unit will follow at
    public Vector2 followOffset;
}
