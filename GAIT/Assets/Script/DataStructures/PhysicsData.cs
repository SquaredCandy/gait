﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsData : MonoBehaviour {

    const float maxRange = 100.0f;

    // Velocity Settings
    // How fast the object accelerates & decelerates
    [Space(10)]
    [Range(0, maxRange)] public float velocityAccel = 5f;
    [Range(0, maxRange)] public float velocityDecel = 5f;

    [SerializeField] [ShowOnly] public float currentVelocity = 0;
    // How fast the object can move
    [Range(0, maxRange)] public float maxVelocity = 5;


    // Rotation Settings
    // How fast the object accelerates & decelerates
    [Space(10)]
    [Range(0, maxRange)] public float rotationAccel = 5.0f;
    [Range(0, maxRange)] public float rotationDecel = 5.0f;

    [SerializeField] [ShowOnly] public float currentRotation = 0;
    // How fast the object can rotate
    [Range(0, maxRange)] public float maxRotation = 5;


    // When to slow down so we don't overshoot
    [Space(10)]
    [Range(0, 10)] public float slowDistance = 0.3f;
    [Range(0, 2)] public float stopDistance = 0.1f;

    [Space(10)]
    // Current velocity of object
    [ShowOnly] public Vector2 velocity;
    // current rotation of object
    [ShowOnly] public float rotation;
}
