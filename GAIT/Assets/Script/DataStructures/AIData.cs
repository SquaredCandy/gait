﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIData : MonoBehaviour {
    
    // The destination for the AI to move to
    public Vector2 destination;
    
    
    // The number of sides of the cirle for debugging
    public int circleSides = 10;

    // The minimum & maximum search radius the ai should go to
    public float minSearchRadius = 2;
    public float maxSearchRadius = 10;

    // Debug
    public bool debugMode = true;
    public bool drawForwardVector = true;
}