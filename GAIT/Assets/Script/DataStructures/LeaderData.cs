﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderData : MonoBehaviour {

    // The radius at which the leader will discover the leader
    public float findPlayerRadius = 5;
    // The radius at which the leader will command the order to fire
    public float fireRadius = 3;

    // Can the followers fire?
    public bool canFire = false;

    // The minimum alpha at which they stop chasing
    public float minAlpha = 0.1f;
    // The stamina drain for the unit
    public float staminaDrain = 0.01f;
}