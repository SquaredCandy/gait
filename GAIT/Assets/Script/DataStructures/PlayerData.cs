﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour {

    [ShowOnly] public int currentLives;
    // Player Lives
    public int maxLives = 0;

    [ShowOnly] public bool invincible;
    [ShowOnly] public float invincibleTimer = 0;
    // Inivisibility Timer
    public float invincibleTime = 2;

    // Flash Speed
    public float flashSpeed = 0.1f;

    // Speed Reduction when invincible
    public float speedReductionFactor = 2;

    // Player score
    [ShowOnly] public int score = 0;

    public Text livesText;

    private int direction = -1;
    private PhysicsData pd;
    private SpriteRenderer sr;
    private Color defaultColor;

    public void Start()
    {
        currentLives = maxLives;
        pd = GetComponent<PhysicsData>();
        sr = GetComponent<SpriteRenderer>();
        defaultColor = sr.color;

        livesText.text = "Deaths: " + currentLives.ToString();
    }

    public void Update()
    {
        // When we get hit we go invincible for some time and slow down
        if (invincible)
        {
            float dt = Time.deltaTime;
            Color newColor = sr.color;

            newColor.a = Mathf.Clamp01(newColor.a + (flashSpeed * direction * dt));
            sr.color = newColor;

            if(newColor.a == 0 || newColor.a == 1)
            {
                direction *= -1;
            }

            invincibleTimer += Time.deltaTime;
            if(invincibleTimer >= invincibleTime)
            {
                invincibleTimer = 0;
                invincible = false;
                sr.color = defaultColor;
                pd.maxVelocity *= speedReductionFactor;
            }
        }
    }

    public void loseLife()
    {
        // Lose a life and go invincible
        if (!invincible)
        {
            ++currentLives;
            invincible = true;
            pd.maxVelocity /= speedReductionFactor;
            livesText.text = "Deaths: " + currentLives.ToString();
        }
    }
}
