﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NamedArraySliderAttribute : PropertyAttribute
{

    public Type TargetEnum;
    public int min;
    public int max;
    public NamedArraySliderAttribute(Type TargetEnum, int min, int max)
    {
        this.TargetEnum = TargetEnum;
        this.min = min;
        this.max = max;
    }
}
