﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public FiniteStateMachine fsm;
    public SteeringMethodScript sms;
    public bool useFSM = false;

    public void Update()
    {
        if (useFSM && fsm != null)
        {
            fsm.UpdateFiniteStateMachine();
        }
        else if (!useFSM && sms != null)
        {
            sms.UpdateSteeringBehaviour();
        }
    }
}
