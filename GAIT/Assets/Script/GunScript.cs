﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour {

    [ShowOnly] public int currentAmmo;
    public int maxAmmo = 5;
    public int ammoCost = 1;

    [ShowOnly] public bool isReloading = false;

    [ShowOnly] public float firingTime = 0.0f;
    public float fireSpeed = 0.5f;
    [ShowOnly] public bool canFire = true;

    public GameObject bullet;

    public PhysicsEngineScript pec;

    public float minAlpha = 0.1f;

	// Use this for initialization
	void Start () {
        currentAmmo = maxAmmo;
	}
	
	// Update is called once per frame
	void Update () {
        if(!canFire && !isReloading)
        {
            firingTime += Time.deltaTime;
            if (firingTime >= fireSpeed)
            {
                firingTime = 0;
                canFire = true;
            }
        }
	}

    public void Fire()
    {
        if (canFire)
        {
            if (currentAmmo > 0)
            {
                var b = Instantiate(bullet) as GameObject;
                b.transform.position = gameObject.transform.position;
                b.transform.rotation = gameObject.transform.rotation;

                PhysicsData pd = b.GetComponent<PhysicsData>();
                pd.currentVelocity = pd.maxVelocity;
                pd.velocity = b.transform.up * pd.currentVelocity;

                SpriteRenderer bsr = b.GetComponent<SpriteRenderer>();
                SpriteRenderer sr = GetComponent<SpriteRenderer>();
                Color newColor = sr.color;
                float oldAlpha = newColor.a;
                newColor.a = 1;
                bsr.color = newColor;
                newColor.a = oldAlpha;
                float alphaRange = 1 - minAlpha;
                float alphaDecrease = alphaRange / maxAmmo;
                newColor.a -= alphaDecrease;
                sr.color = newColor;

                currentAmmo -= ammoCost;
                if (currentAmmo == 0) { isReloading = true; }
                canFire = false;

                pec.physicsObjects.Add(b);
            }
        }
    }

    public void Reload()
    {
        currentAmmo = maxAmmo;
        canFire = true;
        isReloading = false;

        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        Color color = sr.color;
        color.a = 1;
        sr.color = color;
    }
}