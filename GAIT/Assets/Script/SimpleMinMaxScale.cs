﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMinMaxScale : MonoBehaviour {

    public float scaleSpeed = 0.2f;
    public float scaleOffset = 0.5f;
    private Vector3 originalScale;
    private float angle = 0;
    private float TwoPi = 2 * Mathf.PI;
    private Transform myTransform;

    void Start()
    {
        myTransform = transform;
        originalScale = myTransform.localScale;
    }

    void Update()
    {
        float a = Mathf.Sin(angle) * scaleOffset;
        angle = (angle + scaleSpeed) % TwoPi;
        myTransform.localScale = new Vector3(originalScale.x + a, 
            originalScale.y + a, originalScale.z + a);
    }
}
