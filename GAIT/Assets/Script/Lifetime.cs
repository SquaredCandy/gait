﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lifetime : MonoBehaviour {

    private SpriteRenderer sr;
    private float dimSpeed;
    public float time = 1;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        dimSpeed = 1 / time;
        Invoke("Deactivate", time);
    }

    void Deactivate()
    {
        Destroy(gameObject);
    }

    void Update () {
        Color newColor = sr.color;
        newColor.a -= dimSpeed * Time.deltaTime;
        sr.color = newColor;
	}
}
