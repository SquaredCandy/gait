﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Decisions/DGetFireCommand")]
public class DGetFireCommand : Decision {

    // Can the object fire?
    public override bool TransitionLogic(FiniteStateMachine fsm)
    {
        return fsm.bd.followingLeader.GetComponent<LeaderData>().canFire;
    }
}
