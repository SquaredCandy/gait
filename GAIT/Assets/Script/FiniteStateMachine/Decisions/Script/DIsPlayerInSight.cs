﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Decisions/DIsPlayerInSight")]
public class DIsPlayerInSight : Decision {

    // Is the player in sight?
    public override bool TransitionLogic(FiniteStateMachine fsm)
    {
        LeaderData ld = fsm.ld;

        GameObject go = GameObject.FindGameObjectWithTag("Player");
        Vector2 myPos = fsm.transform.position;
        Vector2 playerPos = go.transform.position;
        float distance = Vector2.Distance(myPos, playerPos);
        if (distance < ld.findPlayerRadius)
        {
            if(!Physics.Linecast(myPos, playerPos))
            {
                return true;
            }
        }
        return false;
    }
}