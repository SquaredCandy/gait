﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Decisions/DInBonusRange")]
public class DInBonusRange : Decision {

    public override bool TransitionLogic(FiniteStateMachine fsm)
    {
        LeaderData ld = fsm.ld;

        GameObject[] gos = GameObject.FindGameObjectsWithTag("Buff");
        Vector2 myPos = fsm.transform.position;
        foreach (GameObject go in gos)
        {
            Vector2 buffPos = go.transform.position;
            float distance = Vector2.Distance(myPos, buffPos);
            if (distance < ld.findPlayerRadius)
            {
                if(!Physics.Linecast(myPos, buffPos))
                {
                    return true;
                }
            }
        }
        return false;
    }
}
