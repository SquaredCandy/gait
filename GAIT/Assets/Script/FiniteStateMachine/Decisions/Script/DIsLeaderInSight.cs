﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Finite State Machine/Decisions/DIsLeaderInSight")]
public class DIsLeaderInSight : Decision
{

    // Is the leader in sight?
    public override bool TransitionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        BasicData bd = fsm.bd;
        Vector2 myPos = fsm.transform.position;

        // We are currently following a leader 
        if (bd.followingLeader != null)
        {
            Vector2 leaderPos = bd.followingLeader.transform.position;
            float distance = Vector2.Distance(myPos, leaderPos);
            if (distance < bd.findLeaderRadius)
            {
                return true;
            }
            else
            {
                bd.followingLeader = null;
                //Pathfinding pf = fsm.pf;
                //pf.finalPath = null;
                return false;
            }
        }
        else
        {
            GameObject[] gos = GameObject.FindGameObjectsWithTag("EnemyLeader");

            foreach (GameObject go in gos)
            {
                Vector2 leaderPos = go.transform.position;
                float distance = Vector2.Distance(myPos, leaderPos);
                if (distance < bd.findLeaderRadius)
                {
                    if(!Physics.Linecast(myPos, leaderPos))
                    {
                        bd.followingLeader = go;
                        bd.followOffset = Random.insideUnitCircle *
                            Random.Range(1, bd.findLeaderRadius);
                        ai.destination = go.transform.position + (Vector3)bd.followOffset;
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
