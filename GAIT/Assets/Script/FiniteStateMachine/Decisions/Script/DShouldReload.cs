﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Decisions/DShouldReload")]
public class DShouldReload : Decision {

    // Should the object reload?
    public override bool TransitionLogic(FiniteStateMachine fsm)
    {
        if (fsm.gameObject.GetComponent<GunScript>().isReloading)
        {
//             Vector2 pos = fsm.transform.position;
//             Pathfinding pf = fsm.pf;
//             GameObject[] gos = GameObject.FindGameObjectsWithTag("Reload Station");
//             int index = 0;
//             float shortestDistance = 9999999.0f;
//             for (int i = 0; i < gos.Length; ++i)
//             {
//                 float distance = Vector2.Distance(pos, gos[i].transform.position);
//                 if (distance < shortestDistance)
//                 {
//                     shortestDistance = distance;
//                     index = i;
//                 }
//             }
// 
//             pf._goal = gos[index].transform.position;
            return true;
            //ai.destination = gos[index].transform.position;
        }
        return false;
    }
}
