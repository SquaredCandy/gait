﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Decisions/DIsInFireRange")]
public class DIsInFireRange : Decision {

    // Is the object within fire range?
    public override bool TransitionLogic(FiniteStateMachine fsm)
    {
        LeaderData ld = fsm.ld;
        GameObject go = GameObject.FindGameObjectWithTag("Player");
        Vector2 myPos = fsm.transform.position;
        Vector2 playerPos = go.transform.position;
        float distance = Vector2.Distance(myPos, playerPos);
        if (distance < ld.fireRadius)
        {
            ld.canFire = true;
            return true;
        }
        ld.canFire = false;
        return false;
    }
}
