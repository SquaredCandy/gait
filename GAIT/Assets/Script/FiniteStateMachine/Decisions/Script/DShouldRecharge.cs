﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Decisions/DShouldRecharge")]
public class DShouldRecharge : Decision {

    // Should the object recharge?
    public override bool TransitionLogic(FiniteStateMachine fsm)
    {
        if(fsm.gameObject.GetComponent<SpriteRenderer>().color.a 
            <= fsm.ld.minAlpha)
        {
//             Vector2 pos = fsm.transform.position;
//             Pathfinding pf = fsm.pf;
//             GameObject[] gos = GameObject.FindGameObjectsWithTag("Recharge Station");
//             int index = 0;
//             float shortestDistance = 9999999.0f;
//             for (int i = 0; i < gos.Length; ++i)
//             {
//                 float distance = Vector2.Distance(pos, gos[i].transform.position);
//                 if (distance < shortestDistance)
//                 {
//                     shortestDistance = distance;
//                     index = i;
//                 }
//             }
//             pf._goal = gos[index].transform.position;
            return true;
        }
        return false;
    }
}