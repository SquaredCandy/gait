﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine : MonoBehaviour {

    public bool run = true;
    public State currentState;
    [ShowOnly] public float timeInState = 0;

    // Scripts that we can use in our action and decision scripts
    [HideInInspector] public AIData ai;
    [HideInInspector] public PhysicsData pd;
    [HideInInspector] public LeaderData ld;
    [HideInInspector] public BasicData bd;
    [HideInInspector] public Pathfinding pf;
    [HideInInspector] public Transform myTransform;

    public void Start()
    {
        ai = GetComponent<AIData>();
        pd = GetComponent<PhysicsData>();
        ld = GetComponent<LeaderData>();
        bd = GetComponent<BasicData>();
        pf = GetComponent<Pathfinding>();
        myTransform = transform;
    }

    // Update the finite state machine
    public void UpdateFiniteStateMachine()
    {
        if(run && currentState != null)
        {
            currentState.UpdateState(this);
        }
        timeInState += Time.deltaTime;
    }

    // Change the state of the object
    public bool ChangeState(State nextState)
    {
        if (currentState != nextState && currentState != null)
        {
            //OnChangedState(currentState, nextState);
            currentState = nextState;
            timeInState = 0;
            return true;
        }
        return false;
    }

    public void OnChangedState(State prevState, State newState)
    {
        Debug.Log(prevState + "->" + newState + "\nTime: " + timeInState);
        timeInState = 0;
    }
}
