﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action : ScriptableObject {

    // Action to do
    public abstract void ActionLogic(FiniteStateMachine fsm);
}
