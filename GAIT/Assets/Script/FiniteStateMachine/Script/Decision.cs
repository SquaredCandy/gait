﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Decision : ScriptableObject {

    // Decision to make
    public abstract bool TransitionLogic(FiniteStateMachine fsm);
}
