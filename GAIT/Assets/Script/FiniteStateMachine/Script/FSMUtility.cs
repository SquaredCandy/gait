﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMUtility {

    // Draw a debug circle
    public static void DrawDebugCircle(float sides, float radius, Vector2 pos, Color color)
    {
        float TwoPi = Mathf.PI * 2;
        float increase = TwoPi / sides;

        float xVal = Mathf.Cos(0) * radius;
        float yVal = Mathf.Sin(0) * radius;
        for (float theta = 0; theta < TwoPi; theta += increase)
        {
            float nextXVal = Mathf.Cos(theta + increase) * radius;
            float nextYVal = Mathf.Sin(theta + increase) * radius;
            Vector2 src = new Vector2(xVal, yVal) + pos;
            Vector2 des = new Vector2(nextXVal, nextYVal) + pos;

            Debug.DrawLine(src, des, color);
            xVal = nextXVal;
            yVal = nextYVal;
        }
    }
}
