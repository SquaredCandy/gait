﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/State")]
public class State : ScriptableObject {

    public Action[] actions;
    public Transition[] transitions;

    // Each update we do the actions then decide if we can change stat
    public void UpdateState(FiniteStateMachine fsm)
    {
        DoActions(fsm);
        CheckTransitions(fsm);
    }

    // Do each attached action
    private void DoActions(FiniteStateMachine fsm)
    {
        foreach (Action action in actions)
        {
            action.ActionLogic(fsm);
        }
    }

    // Check each transition to see if we can change state
    private void CheckTransitions(FiniteStateMachine fsm)
    {
        foreach (Transition transition in transitions)
        {
            if (transition.decision.TransitionLogic(fsm))
            {
                if(fsm.ChangeState(transition.trueState)) { break; }
            }
            else
            {
                if(fsm.ChangeState(transition.falseState)) { break; }
            }
        }
    }
}