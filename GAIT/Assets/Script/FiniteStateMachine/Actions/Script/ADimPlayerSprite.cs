﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ADimPlayerSprite")]
public class ADimPlayerSprite : Action {

    // Slowly reduce the alpha of the object
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        LeaderData ld = fsm.ld;
        SpriteRenderer sr = fsm.gameObject.GetComponent<SpriteRenderer>();
        Color newColor = sr.color;
        newColor.a -= ld.staminaDrain * Time.deltaTime;
        if (newColor.a <= ld.minAlpha)
        {
            newColor.a = ld.minAlpha;
        }
        sr.color = newColor;
    }
}