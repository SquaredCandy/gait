﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/AReload")]
public class AReload : Action {

    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        PhysicsData pd = fsm.pd;
        Vector2 pos = fsm.transform.position;
        GunScript gs = fsm.gameObject.GetComponent<GunScript>();

        if(Vector2.Distance(pos, ai.destination) < pd.stopDistance)
        {
            if(gs.isReloading) gs.Reload();
        }
    }
}