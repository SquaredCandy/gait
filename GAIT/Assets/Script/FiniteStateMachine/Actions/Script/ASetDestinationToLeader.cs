﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ASetDestinationToLeader")]
public class ASetDestinationToLeader : Action {

    // Set the destination to the leader
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        BasicData bd = fsm.bd;
        //Pathfinding pf = fsm.pf;
        //ai.destination = pf.GetFirstNode();
        ai.destination = bd.followOffset + (Vector2)bd.followingLeader.transform.position;
    }
}