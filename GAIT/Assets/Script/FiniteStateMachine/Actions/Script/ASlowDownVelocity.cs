﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ASlowDownVelocity")]
public class ASlowDownVelocity : Action {

    // Slow the current object
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        PhysicsData physics = fsm.pd;
        Transform myTransform = fsm.myTransform;

        physics.currentVelocity = PhysicsUtility.LerpFloat(
            physics.currentVelocity, 0, physics.velocityDecel);

        physics.velocity = myTransform.up * physics.currentVelocity * Time.deltaTime;
    }
}