﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/AStopRotation")]
public class AStopRotation : Action {

    // Stop the object from rotating
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        PhysicsData pd = fsm.pd;
        pd.currentRotation = 0;
        pd.rotation = 0;
    }
}
