﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ASetFireCommand")]
public class ASetFireCommand : Action {

    // Set the fire command to true
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        LeaderData ld = fsm.ld;
        ld.canFire = true;
    }
}
