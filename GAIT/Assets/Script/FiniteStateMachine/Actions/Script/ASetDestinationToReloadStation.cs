﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ASetDestinationToReloadStation")]
public class ASetDestinationToReloadStation : Action {

    // Set the destination to the reload station
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        //Pathfinding pf = fsm.pf;
        //ai.destination = pf.GetFirstNode();
        Vector2 pos = fsm.transform.position;
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Reload Station");
        int index = 0;
        float shortestDistance = 9999999.0f;
        for(int i = 0; i < gos.Length; ++i)
        {
            float distance = Vector2.Distance(pos, gos[i].transform.position);
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                index = i;
            }
        }

        ai.destination = gos[index].transform.position;
    }
}
