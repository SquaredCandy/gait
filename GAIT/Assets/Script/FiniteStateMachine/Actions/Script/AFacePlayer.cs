﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/AFacePlayer")]
public class AFacePlayer : Action {

    // Face the object to the player
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        Transform myTransform = fsm.transform;
        PhysicsData physics = fsm.pd;
        Vector2 myPos = myTransform.position;
        Vector2 destination = GameObject.FindGameObjectWithTag("Player").transform.position;
        float distance = Vector2.Distance(destination, myPos);

        PhysicsUtility.FacePosition(myTransform, destination, distance, physics);
    }
}