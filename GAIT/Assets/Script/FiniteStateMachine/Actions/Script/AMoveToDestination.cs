﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/AMoveToDestination")]
public class AMoveToDestination : Action {

    // Move to ai.destination
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        Transform myTransform = fsm.transform;
        PhysicsData physics = fsm.pd;
        Vector2 myPos = myTransform.position;

        float distance = Vector2.Distance(ai.destination, myPos);

        PhysicsUtility.FacePosition(myTransform, ai.destination, distance, physics);
        PhysicsUtility.MoveToPosition(myTransform, distance, physics);
    }
}
