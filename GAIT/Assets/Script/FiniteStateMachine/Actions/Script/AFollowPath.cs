﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/AFollowPath")]
public class AFollowPath : Action {

    public override void ActionLogic(FiniteStateMachine fsm)
    {
        Pathfinding pf = fsm.pf;

        Transform myTransform = fsm.transform;
        PhysicsData physics = fsm.pd;
        Vector2 myPos = myTransform.position;
//         if (pf.finalPath == null || pf.index >= pf.finalPath.Count)
//         {
//             pf._goal = Random.insideUnitCircle * Random.Range(95, 95);
//             pf.UpdateNodes();
//             return;
//         }

        Node nextNode = pf._grid._path[pf._grid.index];
        Vector3 goalNode = nextNode.GetPosition();
        Debug.DrawLine(myPos, goalNode);
        float distance = Vector2.Distance(goalNode, myPos);

        PhysicsUtility.FacePosition(myTransform, goalNode, distance, physics);
        PhysicsUtility.MoveToPosition(myTransform, distance, physics);

        if(distance < physics.stopDistance)
        {
            ++pf._grid.index;
        }
    }
}
