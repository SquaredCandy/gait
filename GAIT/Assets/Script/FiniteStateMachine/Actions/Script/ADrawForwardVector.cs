﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ADrawForwardVector")]
public class ADrawForwardVector : Action {

    // Draw forward vector
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        if (!fsm.ai.drawForwardVector) return;
        Transform myTransform = fsm.transform;
        Vector3 myPos = myTransform.position;
        Debug.DrawLine(myPos, myPos + myTransform.up, fsm.gameObject.GetComponent<SpriteRenderer>().color);
    }
}
