﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/AFire")]
public class AFire : Action {

    // Make the current object fire
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        fsm.gameObject.GetComponent<GunScript>().Fire();
    }
}
