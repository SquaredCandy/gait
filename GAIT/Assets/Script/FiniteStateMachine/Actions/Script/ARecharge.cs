﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ARecharge")]
public class ARecharge : Action {

    // Recharge the current object
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        PhysicsData pd = fsm.pd;
        Vector2 pos = fsm.transform.position;
        SpriteRenderer sr = fsm.gameObject.GetComponent<SpriteRenderer>();

        if (Vector2.Distance(pos, ai.destination) < pd.stopDistance)
        {
            Color newColor = sr.color;
            newColor.a = 1;
            sr.color = newColor;
        }
    }
}
