﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ADrawFindLeaderRadius")]
public class ADrawFindLeaderRadius : Action {

    // Draw a debug circle for leader radius
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        if (!ai.debugMode) return;
        BasicData bd = fsm.bd;
        Transform myTransform = fsm.myTransform;
        FSMUtility.DrawDebugCircle(ai.circleSides, bd.findLeaderRadius,
            myTransform.position, Color.blue);
    }
}
