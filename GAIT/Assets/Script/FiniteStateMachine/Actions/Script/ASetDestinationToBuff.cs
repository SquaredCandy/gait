﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ASetDestinationToBuff")]
public class ASetDestinationToBuff : Action {

    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        Vector2 pos = fsm.transform.position;
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Buff");
        if(gos != null)
        {
            int index = 0;
            float shortestDistance = 9999999.0f;
            for (int i = 0; i < gos.Length; ++i)
            {
                if(gos[i] == null) continue;
                float distance = Vector2.Distance(pos, gos[i].transform.position);
                if (distance < shortestDistance)
                {
                    shortestDistance = distance;
                    index = i;
                }
            }
            if (index < gos.Length && gos[index] != null)
            {
                ai.destination = gos[index].transform.position;
            }
        }
    }
}
