﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ASetDestinationToRandom")]
public class ASetDestinationToRandom : Action {

    // Set the destination to a random point
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        PhysicsData physics = fsm.pd;
        //Pathfinding pf = fsm.pf;
        Vector2 pos = fsm.transform.position;
        float distance = Vector2.Distance(ai.destination, pos);
        if (distance < physics.stopDistance)
        {
            ai.destination = Random.insideUnitCircle *
                Random.Range(ai.minSearchRadius, ai.maxSearchRadius);
//             while(!Physics.Raycast(new Ray(pos, ai.destination - pos), distance))
//             {
//                 ai.destination = Random.insideUnitCircle *
//                     Random.Range(ai.minSearchRadius, ai.maxSearchRadius);
//                 distance = Vector2.Distance(ai.destination, pos);
//             }
        }
    }
}