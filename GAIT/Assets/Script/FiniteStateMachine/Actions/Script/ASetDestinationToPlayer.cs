﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ASetDestinationToPlayer")]
public class ASetDestinationToPlayer : Action {

    // Set the destination to the player
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
//         Pathfinding pf = fsm.pf;
//         ai.destination = pf.GetFirstNode();
        ai.destination = GameObject.FindGameObjectWithTag("Player").transform.position;
    }
}
