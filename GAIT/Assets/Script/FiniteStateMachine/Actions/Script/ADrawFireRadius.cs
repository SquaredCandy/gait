﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Finite State Machine/Actions/ADrawFireRadius")]
public class ADrawFireRadius : Action {

    // Draw a debug circle for fire radius
    public override void ActionLogic(FiniteStateMachine fsm)
    {
        AIData ai = fsm.ai;
        if (!ai.debugMode) return;
        LeaderData ld = fsm.ld;
        Transform myTransform = fsm.transform;
        FSMUtility.DrawDebugCircle(ai.circleSides, ld.fireRadius,
            myTransform.position, Color.green);
    }
}
