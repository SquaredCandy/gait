﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixCameraRotation : MonoBehaviour {

    public Transform parentTransform;
    private Transform myTransform;

	// Use this for initialization
	void Start () {
        myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pPos = parentTransform.position;
        pPos.z = -15;
        myTransform.position = pPos;
	}
}
