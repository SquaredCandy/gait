﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawConnectingTo : MonoBehaviour {

    private Transform myTransform;
    public BasicData bd;

	// Use this for initialization
	void Start () {
        myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		if(bd.followingLeader != null)
        {
            Debug.DrawLine(myTransform.position, bd.followingLeader.transform.position);
        }
	}
}
