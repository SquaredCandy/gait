﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotation : MonoBehaviour {

    public float rotationSpeed = 1;
    private Transform myTransform;

    void Start()
    {
        myTransform = transform;
    }

    void Update()
    {
        Vector3 rot = myTransform.eulerAngles;
        rot.z = (rot.z + (rotationSpeed * Time.deltaTime)) % 360;
        myTransform.eulerAngles = rot;
    }
}
