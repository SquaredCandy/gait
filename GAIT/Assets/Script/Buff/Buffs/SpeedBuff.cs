﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBuff : Buff
{
    protected Color buffColor = Color.magenta;
    protected float speedFactor = 2;

    private SpriteRenderer sr;
    private LeaderData ld;
    private PhysicsData pd;
    private Color oldColor;
    private Color updateColor;
    private float oldStaminaDrain;

    public override int getID()
    {
        return 0;
    }

    public override void onBuffStart(BuffList bl)
    {
        pd = bl.gameObject.GetComponent<PhysicsData>();
        sr = bl.gameObject.GetComponent<SpriteRenderer>();
        ld = bl.gameObject.GetComponent<LeaderData>();

        // We retain the old color
        oldColor = sr.color;
        // Set its alpha back to full (Its stamina back to full)
        oldColor.a = 1;

        // Update its color to the buff color
        sr.color = buffColor;
        updateColor = buffColor;

        // Update its velocity 
        pd.maxVelocity *= speedFactor;

        // Set the duration
        currentDuration = startDuration;

        // Leaders don't drain stamina during the buff
        if(ld != null)
        {
            oldStaminaDrain = ld.staminaDrain;
            ld.staminaDrain = 0;
        }
    }

    public override void updateBuff(BuffList bl)
    {
        // Slowly revert is color back to normal
        float dt = Time.deltaTime;
        updateColor = Color.Lerp(updateColor, oldColor, dt / startDuration);
        sr.color = updateColor;
        currentDuration -= dt;
    }

    public override void onBuffEnd(BuffList bl)
    {
        // Revert to the old settings
        sr.color = oldColor;
        pd.maxVelocity /= speedFactor;
        if(ld != null)
        {
            ld.staminaDrain = oldStaminaDrain;
        }
    }
}
