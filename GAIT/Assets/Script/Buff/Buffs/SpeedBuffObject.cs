﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Buff/SpeedBuff")]
public class SpeedBuffObject : BuffObject
{
    public override Buff GetBuff()
    {
        return CreateInstance<SpeedBuff>();
    }
}