﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BuffObject : ScriptableObject {

    public abstract Buff GetBuff();
}
