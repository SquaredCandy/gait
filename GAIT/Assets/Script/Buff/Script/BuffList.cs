﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffList : MonoBehaviour {

    public List<Buff> buffs;
	
    // Each update we update each buff and if it runs out we end the buff
	void Update () {
        float dt = Time.deltaTime;
		for(int i = buffs.Count - 1; i >= 0; --i)
        {
            buffs[i].updateBuff(this);
            if(buffs[i].currentDuration <= 0)
            {
                buffs[i].onBuffEnd(this);
                buffs.RemoveAt(i);
            }
        }
	}

    // Add the buff to each nearly leader/basic unit
    public void addBuff(BuffObject buffObject)
    {
        RaycastHit2D[] allHit = Physics2D.CircleCastAll(
            gameObject.transform.position, 
            GetComponent<LeaderData>().findPlayerRadius, 
            gameObject.transform.forward);

        foreach(RaycastHit2D hit in allHit)
        {
            GameObject go = hit.collider.gameObject;
            BuffList bl = go.GetComponent<BuffList>();
            if (bl != null)
            {
                Buff buff = buffObject.GetBuff();
                bl.addBuffToList(buff);
            }
        }

    }

    // Initialize the buff and add it to the list
    public void addBuffToList(Buff buff)
    {
        bool buffed = false;
        int i;
        for (i = 0; i < buffs.Count; ++i)
        {
            if(buffs[i].getID() == buff.getID())
            {
                buffed = true;
                break;
            }
        }

        if(buffed)
        {
            buffs[i].onBuffEnd(this);
            buffs[i].onBuffStart(this);
        }
        else
        {
            buff.onBuffStart(this);
            buffs.Add(buff);
        }
    }
}
