﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Buff : ScriptableObject {

    public int ID;
    public float startDuration = 5;
    public float currentDuration { get; protected set; }

    // Get Buff ID
    public abstract int getID();
    // Do on start of buff
    public abstract void onBuffStart(BuffList bl);
    // Do each update
    public abstract void updateBuff(BuffList bl);
    // Do at the end of the buff
    public abstract void onBuffEnd(BuffList bl);
}