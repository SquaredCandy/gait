﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffApplier : MonoBehaviour {

    public BuffObject buffToApply;

    // Add the buff to the Enemy Leader that touched it
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "EnemyLeader")
        {
            col.gameObject.GetComponent<BuffList>().addBuff(buffToApply);
            Destroy(gameObject);
        }
    }
}
