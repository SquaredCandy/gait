﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveAlongPath : MonoBehaviour
{
    public Transform t;
    public NodeGrid grid;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(grid._path != null && grid.index < grid._path.Count && grid._path.Count > 0)
        {
            Vector3 nextPos = grid._path[grid.index].GetPosition();
            Vector3 lerpPos = Vector3.Lerp(t.position, nextPos, Time.deltaTime * 5);
            lerpPos.z = 0;
            t.position = lerpPos;
            if (Vector3.Distance(t.position, nextPos) <= 1)
            {
                ++grid.index;
            }
        }
    }
}
