﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

    private Vector2 _position;

    private int _x;
    private int _y;
    private int _gCost;
    private int _hCost;

    public Node _cameFrom;

    private bool _isWalkable;

    private const float RADIUS = 0.5F;
    private const float DIAMETER = RADIUS * 2;
    private const float GAP = DIAMETER - (RADIUS / 5);

    public Node(Vector2 position, bool isWalkable, int x, int y)
    {
        _position = position;
        _isWalkable = isWalkable;
        _x = x;
        _y = y;
    }

    public void SetX(int x)
    {
        _x = x;
    }
    public void SetY(int y)
    {
        _y = y;
    }

    public void SetGCost(int gCost)
    {
        _gCost = gCost;
    }

    public void SetHCost(int hCost)
    {
        _hCost = hCost;
    }

    public void SetCameFrom(Node cameFrom)
    {
        _cameFrom = cameFrom;
    }

    public Vector2 GetPosition()
    {
        return _position;
    }

    public int GetX()
    {
        return _x;
    }

    public int GetY()
    {
        return _y;
    }

    public int GetGCost()
    {
        return _gCost;
    }

    public int GetHCost()
    {
        return _hCost;
    }

    public int GetFCost()
    {
        return _gCost + _hCost;
    }

    public Node GetCameFrom()
    {
        return _cameFrom;
    }

    public bool IsWalkable()
    {
        return _isWalkable;
    }

    public Vector3 GetVector3Gap()
    {
        return (new Vector3(1, 1, 1) * GAP);
    }

    public static float GetRadius()
    {
        return RADIUS;
    }

    public static float GetDiameter()
    {
        return DIAMETER;
    }

}
