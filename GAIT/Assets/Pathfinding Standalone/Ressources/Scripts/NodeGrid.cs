﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeGrid : MonoBehaviour {

    public Vector2   _scene;
    public LayerMask _obstacleLayerMask;

    private int _sizeX;
    private int _sizeY;

    private Vector2   _origin;
    private Vector2[] _adjacents;

    private Node[,] _nodeGrid;

    public List<Node> _path;

    public int index = 0;

    private void Awake()
    {
        this.InitializeNodeGridSize();
        this.ComputeOrigin();
        this.CreateNodeGrid();
        _adjacents = new Vector2[] {
            new Vector2(1, 0),
            new Vector2(-1, 0),
            new Vector2(0, 1),
            new Vector2(0, -1),
            new Vector2(-1, -1),
            new Vector2(-1, 1),
            new Vector2(1, -1),
            new Vector2(1, 1)
        };
    }

    private void InitializeNodeGridSize()
    {
        _sizeX = Mathf.RoundToInt(_scene.x / Node.GetDiameter());
        _sizeY = Mathf.RoundToInt(_scene.y / Node.GetDiameter());
    }

    private void ComputeOrigin()
    {
        _origin = new Vector2(0.5F - (_scene.x / 2), 0.5F - (_scene.y / 2));
    }
	
    private void CreateNodeGrid()
    {
        _nodeGrid = new Node[_sizeY, _sizeX];

        for (int y = 0; y < _sizeY; y++)
        {
            for (int x = 0; x < _sizeX; x++)
            {
                Vector2 position = new Vector2(_origin.x + x, _origin.y + y);
                bool isWalkable = !(Physics.CheckSphere(position, Node.GetRadius(), _obstacleLayerMask));
                _nodeGrid[x, y] = new Node(position, isWalkable, x, y);
            }
        }
    }

    public List<Node> GetAdjacentsNodes(Node node)
    {
        int adjacentsSize = _adjacents.Length;
        List<Node> adjacents = new List<Node>();

        for (int i = 0; i < adjacentsSize; i++)
        {
            int isX = node.GetX() + (int)_adjacents[i].x;
            int isY = node.GetY() + (int)_adjacents[i].y;
            if (isX >= 0 && isX < _sizeX && isY >= 0 && isY < _sizeY)
            {
                if (_nodeGrid[isX, isY].IsWalkable())
                {
                    adjacents.Add(_nodeGrid[isX, isY]);
                }
            }
        }
        return adjacents;
    }

    public Node Vector3ToNode(Vector3 toConvert)
    {
        float percentX = Mathf.Clamp01((toConvert.x + _scene.x / 2) / _scene.x);
        float percentY = Mathf.Clamp01((toConvert.y + _scene.y / 2) / _scene.y);
        int x = Mathf.RoundToInt((_sizeX - 1) * percentX);
        int y = Mathf.RoundToInt((_sizeY - 1) * percentY);
        return _nodeGrid[x, y];
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position,
                            new Vector3(_scene.x, _scene.y, 1));
        
        if (_nodeGrid != null)
        {
            foreach (Node node in _nodeGrid)
            {
                if (node.IsWalkable())
                {
                    Gizmos.color = Color.white;
                } else {
                    Gizmos.color = Color.red;
                }
                if (_path != null)
                {
                    if (_path.Contains(node))
                    {
                        Gizmos.color = Color.black;
                    }
                }
                Gizmos.DrawCube(node.GetPosition(), node.GetVector3Gap());
            }
        }
    }

}
