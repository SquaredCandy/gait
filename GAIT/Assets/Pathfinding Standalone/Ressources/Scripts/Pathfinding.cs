﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    public Transform _start;
    public Transform _goal;

    private Node _startNode;
    private Node _goalNode;

    public NodeGrid _grid;

    private const int DIAGONAL_VALUE = 7;
    private const int LINEAR_VALUE = 5;

    private void Awake()
    {
        _grid = GetComponent<NodeGrid>();
    }

    private void Update()
    {
        this.GetNodePositions();
        this.ComputePath();
        this.GetFirstNode();
        _grid.index = 0;
    }

    private void GetNodePositions()
    {        
        _startNode = _grid.Vector3ToNode(_start.position);
        _goalNode = _grid.Vector3ToNode(_goal.position);
    }

    private List<Node> GetFinalPath()
    {
        Node current = _goalNode;
        List<Node> finalPath = new List<Node> {};

        while (current != null && current != _startNode)
        {
            finalPath.Add(current);
            current = current.GetCameFrom();
        }
        finalPath.Reverse();
        _grid._path = finalPath;
        return finalPath;
    }

    public Vector2 GetFirstNode()
    {
        List<Node> finalPath = this.GetFinalPath();
        int pathSize = finalPath.Count;
        
        if (pathSize > 0) {
            if (pathSize > 1) {
                return finalPath[1].GetPosition();
            } else {
                return finalPath[0].GetPosition();
            }
        }
        return new Vector2(0, 0);
    }

    private Node GetLowestFCost(List<Node> nodes)
    {
        int listSize = nodes.Count;
        Node lowest = nodes[0];

        for (int i = 0; i < listSize; i++)
        {
            if (nodes[i].GetFCost() < lowest.GetFCost())
            {
                lowest = nodes[i];
            }
        }
        return lowest;
    }
    private int GetDistanceBetween(Node startNode, Node endNode)
    {
        int distanceX = Mathf.Abs(startNode.GetX() - endNode.GetX());
        int distanceY = Mathf.Abs(startNode.GetY() - endNode.GetY());

        if (distanceX > distanceY)
        {
            return DIAGONAL_VALUE * distanceY + LINEAR_VALUE * (distanceX - distanceY);
        }
        return DIAGONAL_VALUE * distanceX + LINEAR_VALUE * (distanceY - distanceX);
    }

    private List<Node> ComputePath()
    {
        Node current;
        List<Node> openList = new List<Node> { _startNode };
        List<Node> closedList = new List<Node>();
        List<Node> adjacentNodes;

        _startNode.SetGCost(0);
        _startNode.SetHCost(this.GetDistanceBetween(_startNode, _goalNode));
        while (openList.Count > 0)
        {
            if ((current = this.GetLowestFCost(openList)) == _goalNode)
            {
                return this.GetFinalPath();
            }
            openList.Remove(current);
            closedList.Add(current);
            adjacentNodes = _grid.GetAdjacentsNodes(current);

            int adjacentSize = adjacentNodes.Count;
            for (int i = 0; i < adjacentSize; i++)
            {
                if (!closedList.Contains(adjacentNodes[i]))
                {
                    int gScoreBuffer = current.GetGCost()
                        + this.GetDistanceBetween(current, adjacentNodes[i]);
                    if (gScoreBuffer < adjacentNodes[i].GetGCost()
                        || !openList.Contains(adjacentNodes[i]))
                    {
                        adjacentNodes[i].SetCameFrom(current);
                        adjacentNodes[i].SetGCost(gScoreBuffer);
                        adjacentNodes[i].SetHCost(
                            this.GetDistanceBetween(adjacentNodes[i], _goalNode));
                        if (!openList.Contains(adjacentNodes[i]))
                        {
                            openList.Add(adjacentNodes[i]);
                        }
                    }
                }
            }
        }
        return null;
    }

}
