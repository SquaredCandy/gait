Games AI Techniques

Group 9
Benjamin Le - s3602668
Jean-Baptiste Sinou - s3673891


The game is not complete as we could not implement 
A* pathfinding into our actual game. So instead,
we have a demo project showing the intended 
implementation outside of the game.

Assignment1 located in (Level/Assignement1) is a 
game where you lead the snitch (Yellow) into the 
goal area. There are Leader (Red) and Basic (Blue) 
enemies that try to stop you. When the 
Score + Deaths = 25 the game will end displaying 
your final score.

workingPathfinding2D located in 
(Pathfinding Standalone/Scenes/workingPathfinding2D)
is a demo of the intended implementation of pathfinding
in our game.